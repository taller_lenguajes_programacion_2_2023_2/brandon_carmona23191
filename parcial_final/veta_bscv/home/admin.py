from django.contrib import admin

from .models import Producto
from .models import TipoProducto

admin.site.register(TipoProducto)
admin.site.register(Producto)
