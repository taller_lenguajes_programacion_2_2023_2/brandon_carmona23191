import pdb
from django.contrib.auth import  login, logout
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from django.urls import reverse
from rest_framework_simplejwt.tokens import RefreshToken
from .forms import RegistroFormulario, LoginFormulario, EmailAuthenticationForm
from .models import Usuario
from django.db import transaction

def registro(request):
    context = {}

    if request.method == 'POST':
        form = RegistroFormulario(request.POST)

        if form.is_valid():
            email = form.cleaned_data['email']

            if Usuario.objects.filter(email=email).exists():
                mensaje = "Este correo electrónico ya está registrado."
                context['form'] = form
                context['mensaje'] = mensaje
            else:

                usuario = form.save()

                user = authenticate(request, email=email, password=form.cleaned_data['password1'])
                if user is not None:
                    login(request, user)
                    home_url = reverse('home')
                    return redirect(home_url)
                else:
                    print("Field Error:", field.name,  field.errors)

        else:
            mensaje = "Error al crear el ususario, porfavor verifique que todos los campos se ingresen de forma correcta"
            context['form'] = form
            context['mensaje'] = mensaje
            for field in form:
                print("Field Error:", field.name,  field.errors)

    else:
        form = RegistroFormulario()
        context['form'] = form

    return render(request, 'loginRegistro/registro.html', context)

def loginVista(request):
    context = {}

    if request.method == 'POST':
        form = EmailAuthenticationForm(request, data=request.POST)

        if form.is_valid():
            user = form.get_user()
            login(request, user)
            refresh = RefreshToken.for_user(user)
            user.token = str(refresh.access_token)
            user.save()

            home = reverse('home')
            return redirect(home)
        else:
            print("Formulario no válido. Errores:")
            print(str(form.errors))
            mensaje = "Error al ingresar. verifique los datos de entrada y vuelva a intentarlo."
            context['form'] = form
            context['mensaje'] = mensaje
           
    else:
        form = EmailAuthenticationForm()
        context['form'] = form

    return render(request, 'loginRegistro/login.html', context)

def logoutVista(request):
    logout(request)
    home = reverse('home')
    return redirect(home)