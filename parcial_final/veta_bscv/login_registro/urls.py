from django.urls import path

from . import views

urlpatterns = [
    path("registro/", views.registro, name="registro"),
    #path('loginRegistro/', views.loginRegistro, name="loginRegistro"),
    path('', views.loginVista, name="login"),
    path('logout/', views.logoutVista, name="logout"),
]