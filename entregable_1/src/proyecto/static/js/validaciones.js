document.addEventListener("DOMContentLoaded", function () {

    const BOTON_REGISTRO = document.getElementById("registrar");
    const NOMBRE = document.getElementById("nombrePersona");
    const APELLIDO = document.getElementById("apellidoPersona");
    const CORREO = document.getElementById("correoElectronico");
    const PASSWORD = document.getElementById("password");



    BOTON_REGISTRO.addEventListener("click", function () {

        

        var valorNombre = NOMBRE.value;
        var valorApellido = APELLIDO.value;
        var valorCorreo = CORREO.value;
        var valorPassword = PASSWORD.value;

        if (valorNombre === "" && valorApellido === "" && valorCorreo === "" && valorPassword === "") {

            var alerta = document.createElement("div");
            alerta.classList.add("alert", "alert-warning", "alert-dismissible", "fade", "show");
            alerta.innerHTML = 'Porfavor ingrese los datos requeridos.';

        } else {

            if (valorNombre === "") {

                alerta.innerHTML = 'Porfavor ingrese su nombre.';

            } if (valorApellido === "") {

                alerta.innerHTML = 'Porfavor ingrese su apellido.';

            } if (valorCorreo === "") {

                alerta.innerHTML = 'Porfavor ingrese el correo electronico.';


            } if (valorPassword === "") {

                alerta.innerHTML = 'Porfavor ingrese la contraseña.';
            }

        }
        document.body.appendChild(alerta);


        setTimeout(function () {
            alerta.classList.remove("show");
            alerta.addEventListener("transitionend", function () {
                alerta.remove();
            });
        }, 5000);

    });

});