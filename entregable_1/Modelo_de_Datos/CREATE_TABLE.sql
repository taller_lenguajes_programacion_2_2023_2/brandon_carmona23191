-- Generado por Oracle SQL Developer Data Modeler 23.1.0.087.0806
--   en:        2023-09-05 22:44:46 COT
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE pedido (
    id_pedido    NUMBER NOT NULL,
    id_persona   NUMBER NOT NULL,
    id_producto  NUMBER NOT NULL,
    id_estado    NUMBER NOT NULL,
    fecha_pedido DATE NOT NULL
);

ALTER TABLE pedido ADD CONSTRAINT pedido_pk PRIMARY KEY ( id_pedido );

CREATE TABLE persona (
    id_persona NUMBER NOT NULL,
    nombre     VARCHAR2(50) NOT NULL,
    apellido   VARCHAR2(50),
    correo     VARCHAR2(60) NOT NULL
);

ALTER TABLE persona ADD CONSTRAINT persona_pk PRIMARY KEY ( id_persona );

CREATE TABLE producto (
    id_producto      NUMBER NOT NULL,
    id_tipo_producto NUMBER NOT NULL,
    talla            VARCHAR2(4) NOT NULL,
    valor            NUMBER NOT NULL
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_producto );

CREATE TABLE tipo_estado_pedido (
    id_estado     NUMBER NOT NULL,
    nombre_estado VARCHAR2(50) NOT NULL
);

ALTER TABLE tipo_estado_pedido ADD CONSTRAINT tipo_estado_pedido_pk PRIMARY KEY ( id_estado );

CREATE TABLE tipo_producto (
    id_tipo_producto NUMBER NOT NULL,
    nombre_producto  NVARCHAR2(50) NOT NULL
);

ALTER TABLE tipo_producto ADD CONSTRAINT tipo_producto_pk PRIMARY KEY ( id_tipo_producto );

CREATE TABLE tipo_usuario (
    id_tipo_usuario NUMBER NOT NULL,
    tipo_usuario    VARCHAR2(50)
);

ALTER TABLE tipo_usuario ADD CONSTRAINT tipo_usuario_pk PRIMARY KEY ( id_tipo_usuario );

CREATE TABLE usuario_acceso (
    id_usuario      NUMBER NOT NULL,
    usuario         VARCHAR2(200) NOT NULL,
    password        VARCHAR2(200) NOT NULL,
    id_tipo_usuario NUMBER NOT NULL,
    id_persona      NUMBER NOT NULL,
    activo          VARCHAR2(1)
);

ALTER TABLE usuario_acceso ADD CONSTRAINT usuario_acceso_pk PRIMARY KEY ( id_usuario );

ALTER TABLE pedido
    ADD CONSTRAINT pedido_persona_fk FOREIGN KEY ( id_persona )
        REFERENCES persona ( id_persona );

ALTER TABLE pedido
    ADD CONSTRAINT pedido_producto_fk FOREIGN KEY ( id_producto )
        REFERENCES producto ( id_producto );

ALTER TABLE pedido
    ADD CONSTRAINT pedido_tipo_estado_pedido_fk FOREIGN KEY ( id_estado )
        REFERENCES tipo_estado_pedido ( id_estado );

ALTER TABLE usuario_acceso
    ADD CONSTRAINT usuario_acceso_persona_fk FOREIGN KEY ( id_persona )
        REFERENCES persona ( id_persona );

ALTER TABLE usuario_acceso
    ADD CONSTRAINT usuario_acceso_tipo_usuario_fk FOREIGN KEY ( id_tipo_usuario )
        REFERENCES tipo_usuario ( id_tipo_usuario );



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             7
-- CREATE INDEX                             0
-- ALTER TABLE                             12
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
