from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("tienda/", views.tienda, name="tienda"),
    path('filtrarProductos/<int:id>',views.filtrarProductos,name="filtrarProductos"),
    path('tienda/producto/<int:idProducto>', views.producto)
]