from django.shortcuts import render, redirect
from .models import Producto, TipoProducto

def home(request):
    return render(request, 'home/home.html')

def tienda(request):

    categorias = TipoProducto.objects.all()
    productos = Producto.objects.all()
    return render(request, 'tienda/tienda.html', {"productos":productos, "categorias":categorias})

def filtrarProductos(request, id):
    productos = Producto.objects.filter(idTipoProducto=id)
    categorias = TipoProducto.objects.all()
    return render(request, 'tienda/tienda.html', {"productos":productos, "categorias":categorias})

def producto(request, idProducto):
    producto = Producto.objects.get(idProducto=idProducto)
    return render(request, 'tienda/producto.html', {"producto":producto})
