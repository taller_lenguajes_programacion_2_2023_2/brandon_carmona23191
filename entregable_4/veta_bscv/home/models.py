from django.db import models

class TipoProducto(models.Model):
    nombreTipo = models.CharField(max_length=1000)

class Producto(models.Model):
    idProducto = models.AutoField(primary_key=True)
    nomrbe = models.CharField(max_length=1000)
    descripcion = models.CharField(max_length=5000)
    idTipoProducto = models.ForeignKey(TipoProducto, on_delete=models.CASCADE)
    presio = models.IntegerField(default = 0)
    imagen = models.CharField(max_length=1000)
