# Taller de Programación 2

## Fechas de Evaluación

1. **Entregable 1**: 6 septiembre 2023
2. **Entregable 2**: 2 de Octubre, 2023
3. **Parcial 1**: 4 Octubre, 2023
4. **Entregable 3**: 6 Noviembre, 2023
5. **Entregable 4**: 29 Noviembre, 2023
6. **Parcial Final**: 6 Diciembre, 2023

## Herramientas Utilizadas

- **Git**: [Sitio oficial](https://git-scm.com/)
- **Visual Studio Code**: [Descargar aquí](https://code.visualstudio.com/) Extenciones (Python , Git grafh , Git lens)
- **Python 3.9.2**: [Python descarga](https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe)
- Datamodeler Oracle [link descarga](https://www.oracle.com/database/sqldeveloper/technologies/sql-data-modeler/download/#license-lightbox)

## Control de Versiones

- **git clone** `<url>`
- **git add** .  Empaquetar cambios
- **git commit -m "descripcion del cambio"** Etiquetar version
- **git push origin main** Carga o empuja el paquete a la ubicacion remota

## Espacio de Trabajo

- **python -m venv venv** creacion de espacio de trabajo
- **.\venv\Scripts\activate** activar entorno virtual

## Comandos VSCode

- Control + ñ Terminal

## enlaces 
https://www.typeitjs.com/
https://scrollrevealjs.org/
https://xsgames.co/animatiss/
https://anijs.github.io/
https://bgjar.com/
https://michalsnik.github.io/aos/
https://mybrandnewlogo.com/es/generador-de-gradiente-de-color
https://codepen.io/tag/background
## Sesiones

| Sesión | Fecha      | Tema                                                               | Referencias |
| ------- | ---------- | ------------------------------------------------------------------ | ----------- |
| 1       | 09/08/2023 | Presentación y Concertación de evaluación                       |             |
| 1       | 14/08/2023 | Introducción al control de versiones y su importancia en el curso |             |
| 2       | 16/08/2023 | Diseño responsive con CSS                                         |             |
| 3       | 23/08/2023 | Modelado de datos                                          |             |