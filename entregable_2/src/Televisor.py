from Casa import Casa
from Conexion import Conexion
import pandas as pd

class Televisor(Casa):
    
    def __init__(self,idCasa=0, pisos=0, habitaciones=0, color="", direccion="", propietario="",idTelevisor=0,marca="",modelo="",precio=0,tipoPantalla="",sistemaOperativo="",calidad=""):
        """Clase que permite crear y y realizar las respectivas operaciones para el televisor

        Args:
            idCasa (int, optional): identificador unico de la clase casa. Dedault to 0
            pisos (int, optional): numero de pisos que contiene la casa. Dedault to 0
            habitaciones (int, optional):numero de habitaciones que contiene la casa. Dedault to 0
            color (str, optional): color de la casa. Dedault to ""
            direccion (str, optional): direccion donde se encuentra la casa. Dedault to ""
            propietario (str, optional): nombre del propietario de la casa. Dedault to ""
            idTelevisor (int, optional): identificador unico de la clase. Defaults to 0.
            marca (str, optional): marca del televisor. Defaults to "".
            modelo (str, optional): modelo del televisor. Defaults to "".
            precio (int, optional): precio del televisor. Defaults to 0.
            tipoPantalla (str, optional): tipo de pantalla que usa el televisor EJ, LED, OLED, etc... . Defaults to "".
            sistemaOperativo (str, optional): sistema operativo que usa el televisor. Defaults to "".
            calidad (str, optional): calidad de la pantalla del televisor. Defaults to "".
        """
        super().__init__(idCasa, pisos, habitaciones, color, direccion, propietario)
        self.__idTelevisor = idTelevisor
        self.__marca = marca
        self.__modelo = modelo
        self.__precio = precio
        self.__tipoPantalla = tipoPantalla
        self.__sistemaOperativo = sistemaOperativo
        self.__calidad = calidad
        self.__conexion = Conexion()
        ruta = "./static/excel/datosTablas.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="Televisor")
        self.crearTablaTelevisor()
        self.insertarNuevoTelevisor()
        self.consultarTelevisores()
        self.actualizarRegistroTelevisor()
        self.eliminarRegistroTelevisor()

    @property
    def _idTelevisor(self):
        return self.__idTelevisor

    @_idTelevisor.setter
    def _idTelevisor(self, value):
        self.__idTelevisor = value

    @property
    def _marca(self):
        return self.__marca

    @_marca.setter
    def _marca(self, value):
        self.__marca = value

    @property
    def _modelo(self):
        return self.__modelo

    @_modelo.setter
    def _modelo(self, value):
        self.__modelo = value

    @property
    def _precio(self):
        return self.__precio

    @_precio.setter
    def _precio(self, value):
        self.__precio = value

    @property
    def _tipoPantalla(self):
        return self.__tipoPantalla

    @_tipoPantalla.setter
    def _tipoPantalla(self, value):
        self.__tipoPantalla = value

    @property
    def _sistemaOperativo(self):
        return self.__sistemaOperativo

    @_sistemaOperativo.setter
    def _sistemaOperativo(self, value):
        self.__sistemaOperativo = value

    @property
    def _calidad(self):
        return self.__calidad

    @_calidad.setter
    def _calidad(self, value):
        self.__calidad = value

    def crearTablaTelevisor(self):

        if self.create(nom_tabla="TELEVISOR",datos_tbl="datosTelevisor"):
            print("Tabla Personas Creada!!!")
        return True
    
    def insertarNuevoTelevisor(self):
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}","{}"'.format(row["ID_CASA"],row["MARCA"],row["MODELO"],row["PRECIO"],row["TIPO_PANTALLA"],row["SISTEMA_OPERATIVO"],row["CALIDAD"])
            self.insert(nom_tabla="TELEVISOR",nom_columns="columnasTelevisor",datos_carga=datos)
        return True
    
    def consultarTelevisores(self):
        datos = self.select(nom_tabla="TELEVISOR")
        print(datos)
        return datos
    
    def actualizarRegistroTelevisor(self):
        print("Por favor ingrese la nueva calidad de pantalla para el televisor")
        calidad = input()
        print("Ahora ingrese el id del televisor que desea actualizar")
        id_televisor=input()
        self.update(nom_tabla="TELEVISOR",registro="ID_TELEVISOR=" + id_televisor, datos="CALIDAD = '" + calidad + "'")
        self.consultarTelevisores()
        return True
    
    def eliminarRegistroTelevisor(self):
        print("Por favor ingrese el id del televisor que desea eliminar")
        id_televisor = input()
        self.delete(nom_tabla="TELEVISOR",registro="ID_TELEVISOR=" + id_televisor)
        self.consultarTelevisores()
        return True