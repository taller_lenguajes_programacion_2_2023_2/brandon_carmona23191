import sqlite3
import json

class Conexion():    
    def __init__(self) -> None:
        self.__user=""
        self.__password=""
        self.__puerto=0
        self.__url=""
        __nom_db="db_bscv_entregable2.sqlite"
        self.__querys = self.obtener_json()
        self.__conex = sqlite3.connect(__nom_db)
        self.__cursor = self.__conex.cursor()

    def obtener_json(self):
            ruta = "./static/sql/querys.json"
            querys={}
            with open(ruta, 'r') as file:
                querys = json.load(file) 
            return querys
    
    def create(self, nom_tabla="",datos_tbl = ""):
        if nom_tabla != "" :
            datos=self.__querys[datos_tbl]
            query =  self.__querys["create_tb"].format(nom_tabla,datos)
            self.__cursor.execute(query)
            return True
        else:
            return False
        
    def insert(self, nom_tabla="",nom_columns = "", datos_carga=""):
        if nom_tabla != "" :
            columnas=self.__querys[nom_columns]
            query =  self.__querys["insert"].format(nom_tabla,columnas,datos_carga)
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False
        
    def select(self, nom_tabla=""):
        datos = ""
        if nom_tabla != "" :
            query =  self.__querys["select"].format(nom_tabla)
            self.__cursor.execute(query)
            datos = self.__cursor.fetchall()
            return datos
        else:
            datos = "Tabla no existente"
            return datos
        
    def update(self, nom_tabla="", registro="", datos=""):
        if nom_tabla != "" :
            query =  self.__querys["update"].format(nom_tabla, datos, registro)
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False
    

    def delete(self, nom_tabla="", registro=""):
        if nom_tabla != "" :
            query =  self.__querys["delete"].format(nom_tabla, registro)
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False