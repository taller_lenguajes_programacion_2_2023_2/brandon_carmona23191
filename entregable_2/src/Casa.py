from Conexion import Conexion
import pandas as pd

class Casa(Conexion):
    def __init__(self, idCasa=0, pisos=0, habitaciones=0, color="", direccion="", propietario="") :

        """Clase que permite crear y y realizar las respectivas operaciones para la casa
        Args:
            idCasa (int, optional): identificador unico de la clase. Dedault to 0
            pisos (int, optional): numero de pisos que contiene la casa. Dedault to 0
            habitaciones (int, optional):numero de habitaciones que contiene la casa. Dedault to 0
            color (str, optional): color de la casa. Dedault to ""
            direccion (str, optional): direccion donde se encuentra la casa. Dedault to ""
            propietario (str, optional): nombre del propietario de la casa. Dedault to ""
        """

        self.__idCasa = idCasa
        self.__pisos = pisos
        self.__habitaciones = habitaciones
        self.__color = color
        self.__direccion = direccion
        self.__propietario = propietario
        super().__init__()
        ruta = "./static/excel/datosTablas.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="Casa")
        self.crearTablaCasa()
        self.insertarNuevaCasa()
        self.consultarCasas()
        self.actualizarRegistro()
        self.eliminarRegistro()

    @property
    def _idCasa(self):
        return self.__idCasa

    @_idCasa.setter
    def _idCasa(self, value):
        self.__idCasa = value

    @property
    def _pisos(self):
        return self.__pisos

    @_pisos.setter
    def _pisos(self, value):
        self.__pisos = value

    @property
    def _habitaciones(self):
        return self.__habitaciones

    @_habitaciones.setter
    def _habitaciones(self, value):
        self.__habitaciones = value

    @property
    def _color(self):
        return self.__color

    @_color.setter
    def _color(self, value):
        self.__color = value

    @property
    def _direccion(self):
        return self.__direccion

    @_direccion.setter
    def _direccion(self, value):
        self.__direccion = value

    @property
    def _propietario(self):
        return self.__propietario

    @_propietario.setter
    def _propietario(self, value):
        self.__propietario = value       

    def crearTablaCasa(self):
        atributos = vars(self)
        if self.create(nom_tabla="CASA",datos_tbl="datosCasa"):
            print("Tabla Casa Creada correctamente")
        return atributos
    
    def insertarNuevaCasa(self):
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}"'.format(row["PISOS"],row["HABITACIONES"],row["COLOR"],row["DIRECCION"],row["PROPIETARIO"])
            self.insert(nom_tabla="CASA",nom_columns="columnasCasa",datos_carga=datos)
        return True
    
    def consultarCasas(self):
        datos = self.select(nom_tabla="CASA")
        print(datos)
        return datos
    
    def actualizarRegistro(self):
        print("Por favor ingrese el nuevo numero de habitaciones de la casa")
        habitaciones = input()
        print("Ahora ingrese el id de la casa la cual desea actualizar")
        id_casa=input()
        self.update(nom_tabla="CASA",registro="ID_CASA=" + id_casa, datos="HABITACIONES=" + habitaciones)
        self.consultarCasas()
        return True
    
    def eliminarRegistro(self):
        print("Por favor ingrese el id de la casa que desea eliminar")
        id_casa = input()
        self.delete(nom_tabla="CASA",registro="ID_CASA="+id_casa)
        self.consultarCasas()
        return True