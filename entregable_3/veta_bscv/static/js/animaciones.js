document.addEventListener("DOMContentLoaded", function () {

    var alertMessage = document.getElementById("mensajeAlertas");
    if (alertMessage) {
        alertMessage.style.display = "block";
        setTimeout(function() {
            alertMessage.style.display = "none";
        }, 5000);
    }
});