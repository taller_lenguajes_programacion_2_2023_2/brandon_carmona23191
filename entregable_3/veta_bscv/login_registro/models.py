from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager, Group, Permission


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError('El campo correo electronico es obligatorio')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.set_username(email)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        return self.create_user(email, password, **extra_fields)
    
class Usuario(AbstractUser):
    id = models.IntegerField(primary_key=True)
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=100,unique=False)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    celular = models.IntegerField()
    direccion = models.CharField(max_length=1000)
    fechaRegistro = models.DateTimeField(auto_now_add=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    groups = models.ManyToManyField(
        Group,
        verbose_name='groups',
        blank=True,
        help_text='The groups this user belongs to.',
        related_name='usuario_groups_set',
        related_query_name='usuario_group',
    )

    user_permissions = models.ManyToManyField(
        Permission,
        verbose_name='user permissions',
        blank=True,
        help_text='Specific permissions for this user.',
        related_name='usuario_permissions_set',
        related_query_name='usuario_permission',
    )

    class Meta:
        permissions = []