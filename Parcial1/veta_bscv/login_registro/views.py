from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Usuario
from .forms import RegistroFormulario, LoginFormulario

def registro(request):
    context={}
    if(request.method=="POST"):
        form = RegistroFormulario(request.POST)
        if form.is_valid():
            form.save()
        else:
            for field in form:
                print("Field Error:", field.name,  field.errors)
    else:
        form=RegistroFormulario()
        context['form'] = form

    return render(request, 'loginRegistro/registro.html', context)

def login(request):

    context={}
    if request.method == 'POST':
        form = LoginFormulario(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            try:
                user = Usuario.objects.get(email=email)
                if user.password == password:
                    request.session.set_expiry(0)
                    request.session['user_id'] = user.idUsuario
                    print("Home")
                    return redirect(home)
                else:
                    messages.error(request, 'Credenciales incorrectas')
            except Usuario.DoesNotExist:
                print("Usuario no existe")
                messages.error(request, 'Usuario no encontrado')
    else:
        form = LoginFormulario()
        context['form'] = form
    return render(request, 'loginRegistro/login.html', context)

def home(request):
    return render(request, 'home/home.html')
