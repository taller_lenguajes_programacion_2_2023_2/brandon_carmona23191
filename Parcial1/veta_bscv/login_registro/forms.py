from django import forms

from .models import Usuario
from datetime import date



class RegistroFormulario(forms.ModelForm):

    class Meta:
        model = Usuario

        fields=[

            'idPersona',
            'nombre',
            'apellido',
            'celular',
            'direccion',
            'email',
            'password',

        ] 
        widgets={

            'nombre': forms.TextInput(attrs={'class':'form-control', 'Placeholder':'Nombre'}),
            'apellido': forms.TextInput(attrs={'class':'form-control', 'Placeholder':'Apellido'}),
            'celular': forms.TextInput(attrs={'class':'form-control','Placeholder':'Celular'}),
            'direccion': forms.TextInput(attrs={'class':'form-control','Placeholder':'Direccion'}),
            'email': forms.TextInput(attrs={'class':'form-control', 'Placeholder':'Correo Electronico'}),
            'password': forms.TextInput(attrs={'type':'password', 'class':'form-control', 'Placeholder':'Contraseña'}),

        }

class LoginFormulario(forms.ModelForm):
    class Meta:
        model = Usuario
        fields=[

            'email',
            'password'

        ] 
        widgets={
            'email': forms.TextInput(attrs={'class':'form-control', 'Placeholder':'Correo Electronico'}),
            'password': forms.TextInput(attrs={'type':'password', 'class':'form-control', 'Placeholder':'Contraseña'})
        }