from django.db import models

class Persona(models.Model):
    idPersona = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=1000)
    apellido = models.CharField(max_length=1000)
    celular = models.IntegerField()
    direccion = models.CharField(max_length=1000)

class Usuario(Persona):
    idUsuario = models.IntegerField(primary_key=True)
    email = models.CharField(max_length=1000)
    password = models.CharField(max_length=1000)
    fechaRegistro = models.DateTimeField(auto_now_add=True)
    token = models.CharField(max_length=1000, blank=True)