from django.contrib import admin

from .models import Persona
from .models import Usuario

admin.site.register(Persona)
admin.site.register(Usuario)    