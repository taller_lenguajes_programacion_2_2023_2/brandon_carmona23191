document.addEventListener("DOMContentLoaded", function () {

    const INGRESO_SISTEMA = document.getElementById("ingresoSistema");
    const REGISTRO_SISTEMA = document.getElementById("registroSistema");

    function init(){
        REGISTRO_SISTEMA.style.display = "none";
        INGRESO_SISTEMA.style.display = "block";
    }

    window.addEventListener("hashchange", function () {
        if (window.location.hash === "#formulario-ingreso") {
            REGISTRO_SISTEMA.style.display = "none";
            INGRESO_SISTEMA.style.display = "block";
        } else if (window.location.hash === "#formulario-registro") {
            INGRESO_SISTEMA.style.display = "none";
            REGISTRO_SISTEMA.style.display = "block";
        }
    });
    init();

});